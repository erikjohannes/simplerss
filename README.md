# SimpleRSS

A simplistic approach to reading news and webfeeds using RSS.

SimpleRSS is a Python script that is run from the command line. It is created
with the following goal:

- **Create an uncluttered view of the most recent articles of your favourite news
  feeds.**

SimpleRSS fetches the most recent articles from your feeds, and present them in
a minimalistic, sleek interface. The goal is to be able to browse news without
the clutter, ads and distracting elements that has become so common for web
pages.

SimpleRSS is not the classic type of RSS reader, it is simply an alternative
frontend to your favourite feeds.


## Requirements

SimpleRSS is just a script, and only requires the following:

- Python 3.8
- feedparser (install with `pip3 install feedparser`)

## Usage

### Quick start

1. Place the URLs of the feeds you want to follow into the file `urls` (1 URL
   per line)
2. Run `python3 simplerss.py`
3. Open `index.html` in a browser

The `urls` file already contains some example URLs.  A file called `index.html`
is generated when running the script in step 2, and this file contains the most
recent articles from your feeds in a minimalistic, sleek interface. 

By clicking the arrow to the left of an article title, details are revealed.
If the RSS feed includes the article text, you can read the whole article in
SimpleRSS, but most of the time feeds only include the ingress.

### Advanced options

The script will by default fetch the 10 latest articles from each feed. To
increase this number to for example 15, run `python3 simplerss.py -n 15`.

The feeds could also be sent to you by e-mail (could be set up as a cron job).
Follow these steps:

1. Place the URLs of the feeds you want to follow into the file `urls` (1 URL
   per line). The file already contains a few example URLs.
2. Place your the credentials of your e-mail account in a file called
   `credentials-email.txt`, with the username on the first line, and the
   password on the second line.
3. Run `python3 simplerss.py -m`.

### Errors

Sometimes, the feedparser will get an error while fetching a feeds, for example
if the feed does not contain enough articles compared to what you have
specified. The script will print that there is an error on a given feed, but
will continue to fetch the rest of the feeds.

## Contributing

Contributions and feedback are welcome! Please open an issue for bug reports,
feature requests etc.
