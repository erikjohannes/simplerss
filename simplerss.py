#!/usr/bin/env python3
# ============================================================================
# File:     simpleerss.py
# Author:   Erik Johannes Husom
# Created:  2020-01-19
# ----------------------------------------------------------------------------
# Description:
#
# ============================================================================
import argparse
import smtplib
import ssl
import time
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import feedparser

CSSLINES = """
/* File:        feed.css
 * Author:      Erik Johannes Husom
 * Date:        2020-01-19
 * Description: Style for RSS feed reader.
 * definitions.
 */

:root {
    --main-font-color: rgb(57, 56, 56);
    --secondary-font-color: rgb(142, 142, 142);
}

/*Body{{{*/
body {
    font-family: 'Avenir', sans-serif;
    font-size: smaller;
    color: var(--main-font-color);
    box-sizing: border-box;
    /* Constraining width, and centering */
    max-width: 90em;
    margin: auto auto 1em auto;
    padding-left: 5%;
    padding-right: 5%;
}

body.wide {
    max-width: none;
}
/*}}}*/

/*Header{{{*/
header {
    top: 0;
    right: 0;
    left: 0;
    padding-top: 5rem;
    padding-bottom: 2rem;
}
/*}}}*/

/*Nav{{{*/
nav {
  font-size: 1em;
  font-weight: 600;
  color: var(--secondary-font-color);
  text-transform: lowercase;
}
/*}}}*/

/*Headers{{{*/
h1 {
    padding-top: 0.2em;
    font-size: 3em;
}

h2 {
    /* padding-top: 0.0001em; */
}

h3 {
    display: inline;
    font-size: smaller;
    font-weight: 300;
}
/*}}}*/

 /*Hr (line){{{*/
hr {
  border: 0.1rem solid rgb(233, 233, 233);
}
/*}}}*/

/*Lists{{{*/
ul {
  /* list-style-type: none; */
  /* padding-left: 0; */
}

li {
  padding-top: 0.1rem;
}
/*}}}*/

/*Links{{{*/
a {
  text-decoration: none;
}

h1 a {
    color:  var(--main-font-color);
}

h2 a {
  color:  var(--main-font-color);
  font-weight: 400;
}

a:hover {
  background: rgba(199, 104, 208, 0.5);
  /* font-weight: 500; */
  /* color: rgb(17, 131, 185); */
}

a:visited {
  /* color:  var(--main-font-color); */
}

/*}}}*/

/*Tablets and mobile devices{{{*/
@media screen and (max-width: 992px) {
  body {  
    max-width: none;
    font-size: larger;
  }
}

/*Laptops and large screens{{{*/
@media screen and (min-width: 992px) {

}
/*}}}*/

/*Footer{{{*/
footer {
  right: 0;
  bottom: 0;
  left: 0;
  margin-top: 2rem;
  padding: 0.5rem;
  text-align: center;
  font-size: 0.6rem;
}
/*}}}*/

button {
    /* width: 5em; */
    /* height: 1em; */
    /* display: inline; */
}
"""


class RSSReader:
    """Reading RSS feeds."""

    def __init__(self, num_entries=5, rssfile="urls.txt"):

        self.num_entries = num_entries
        self.rssfile = rssfile

        self.read_feed_urls()
        self.parse_feeds()
        self.create_html_feeds()

    def read_feed_urls(self):
        """Read the RSS URLs from file."""

        with open(self.rssfile, "r") as infile:
            self.feed_urls = [line.rstrip("\n") for line in infile]

    def parse_feeds(self):
        """Use feedparser to parse RSS feeds from URLs."""

        self.feeds = [feedparser.parse(url) for url in self.feed_urls]

    def print_feeds(self):
        """Print RSS feeds."""

        print("RSS READER")

        for feed, url in zip(self.feeds, self.feed_urls):
            try:
                print("")
                print("====================================")
                cprint(feed.feed.title.upper())
                for i in range(self.num_entries):
                    print("")
                    entry = feed.entries[i]
                    try:
                        cprint(entry.title, style="subtitle")
                    except:
                        print("Title not found.")
                    try:
                        cprint(entry.link, style="link")
                    except:
                        print("Link not found.")
                    try:
                        cprint("[" + entry.published + "]", style="date")
                    except:
                        pass
            except:
                print(f"Error on feed {url}")

    def add_feed(self, url):
        """Add RSS url to the file containing all RSS feeds.

        Parameters
        ----------
        url : string
            The URL of the RSS feed to be added.

        """

        with open(self.rssfile, "a") as infile:
            infile.write(url)

    def create_html_feeds(self):

        # timestamp = time.strftime("%Y%m%d-%H%M")

        # with open("rssfeeds-" + timestamp + ".html", "w") as f:
        with open("index.html", "w") as f:
            f.write("<!doctype html>")
            f.write("<html>")
            f.write("<head>")
            f.write("\n")
            f.write("<base target='_blank'>")
            f.write("\n")
            f.write("<script>")
            f.write("\n")
            f.write(
                """
function toggleDisplayHide(divID) {
  var x = document.getElementById(divID);
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}"""
            )
            f.write("\n")
            f.write("</script>")
            f.write("\n")
            f.write("<style>")

            # with open("rssfeed.css", "r") as css:
            #     CSSLINES = css.readlines()

            f.writelines(CSSLINES)
            f.write("</style>")
            f.write("</head>")
            f.write("\n")

            article_id = 0

            for feed, url in zip(self.feeds, self.feed_urls):

                try:
                    f.write(
                        htmlstr(htmlstr(feed.feed.title, "a", url=feed.feed.link), "h1")
                    )
                    f.write("\n")
                    for i in range(self.num_entries):
                        entry = feed.entries[i]
                        f.write("<details>")
                        f.write("\n")
                        try:
                            f.write(
                                htmlstr(
                                    htmlstr(entry.title, "a", url=entry.link), "summary"
                                )
                            )
                            f.write("\n")
                            # f.write("<button onclick=toggleDisplayHide({})>TOGGLE</button>".format(article_id))
                        except:
                            f.write(htmlstr("Title not found.", "summary"))
                            f.write("\n")
                        try:
                            f.write(htmlstr(entry.published, "h3"))
                            f.write("\n")
                        except:
                            f.write(htmlstr("Published date not found.", "summary"))
                            f.write("\n")
                        try:
                            f.write(htmlstr(str(entry.summary), "p"))
                            # f.write(htmlstr(str(entry.summary), "article",
                            #     metadata="id={} style='display:none;'".format(article_id)))
                        except:
                            print(f"Error on feed {url}")

                        f.write("</details>")
                        f.write("\n")
                        article_id += 1
                except:
                    print(f"Error on feed {url}")

            f.write("</html>")

    def mail_feed(self):

        with open("index.html", "r") as f:
            message = f.read()

        send_email("RSS feeds", message, addressee=0, password_method="file")


def htmlstr(string, tag, metadata=None, url=None):

    if metadata == None:
        starttag = "<" + tag + ">"
    else:
        starttag = "<" + tag + " " + metadata + ">"

    endtag = "</" + tag + ">"

    if tag == "a" and url != None:
        starttag = "<" + tag + " href=" + str(url) + " >"

    return starttag + str(string) + endtag


# Format: \033[<code>;<code>;<code>m
#
# COLOR     | TEXT  | BACKGROUND
# ----------|-------|-----------
# Black     | 30    | 40
# Red       | 31    | 41
# Green     | 32    | 42
# Yellow    | 33    | 43
# Blue      | 34    | 44
# Purple    | 35    | 45
# Cyan      | 36    | 46
# White     | 37    | 47
#
# TEXT STYLE
# ----------
# No effect:    0
# Bold:         1
# Underline:    2
# Italics:      3
# Blinking:     5
# ============================================================================

init = "\033["
default = init + "m"
date = init + "3;35m"
title = init + "1;36m"
subtitle = init + "0;37m"
link = init + "3;34m"


def cprint(string, style="title", end="\n"):

    if style == "date":
        style = date
    elif style == "title":
        style = title
    elif style == "subtitle":
        style = subtitle
    elif style == "link":
        style = link

    print(style + string + default, end=end)


def read_credentials_from_cli():
    """Read username and password from command line."""
    username = input("Username=")
    password = input("Password=")

    return username, password


def read_credentials_from_file(filename):
    """Read username and password from file."""

    with open(filename, "r") as infile:
        username = infile.readline().strip()
        password = infile.readline().strip()

    return username, password


def send_email(subject, content, addressee=0, password_method="cli"):
    """
    Send e-mail using smtp.

    Parameters
    ----------
    subject : subject of email
    message : main body of email
    addressee : reciever of email (default=0, sending to yourself.)

    """

    smtpObj = smtplib.SMTP("smtp.online.no", 587)
    context = ssl.create_default_context()
    smtpObj.starttls(context=context)

    if password_method == "file":
        try:
            username, password = read_credentials_from_file("credentials-email.txt")
        except FileNotFoundError:
            print(
                "Credentials to e-mail account should be placed in a file called ´credentials-email.txt´. Username on first line, password on second line."
            )
    else:
        username, password = read_credentials_from_cli("credentials-email.txt")

    smtpObj.login(username, password)

    if addressee == 0:
        addressee = username

    message = MIMEMultipart("alternative")
    message["Subject"] = subject
    message["From"] = username
    message["To"] = addressee
    message.attach(MIMEText(content, "html"))

    # smtpObj.sendmail(username, addressee, 'Subject: {:s}\n{:s}'.format(subject, content))
    smtpObj.sendmail(username, addressee, message.as_string())

    smtpObj.quit()


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-f", "--file", default="urls", help="File containing one RSS url per line."
    )
    parser.add_argument(
        "-n", default=10, type=int, help="Number of articles to display per feed."
    )
    parser.add_argument(
        "-m", "--mail", action="store_true", help="Send RSS feeds as e-mail."
    )

    args = parser.parse_args()

    rssreader = RSSReader(args.n, args.file)

    if args.mail:
        rssreader.mail_feed()

    # rssreader.print_feeds()
